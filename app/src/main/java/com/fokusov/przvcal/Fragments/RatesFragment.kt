package com.fokusov.przvcal.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fokusov.przvcal.Controller.Cal
import com.fokusov.przvcal.Model.Constants

import com.fokusov.przvcal.R


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RatesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 */
class RatesFragment : Fragment() {

    private lateinit var myPref : SharedPreferences

    private var mListener: OnFragmentInteractionListener? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_rates, container, false)
        myPref = Cal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)

        var ratesArr = Constants.rates_data_RU
        val prefAppCoutry = myPref.getInt("app_coutry", 0)

        when(prefAppCoutry){
            0 -> ratesArr = Constants.rates_data_RU
            1 -> ratesArr = Constants.rates_data_BE
            2 -> ratesArr = Constants.rates_data_UA
        }

        val y1cal = rootView.findViewById<TextView>(R.id.y1cal)
        val y1work = rootView.findViewById<TextView>(R.id.y1work)
        val y1vac = rootView.findViewById<TextView>(R.id.y1vac)
        val y1_24 = rootView.findViewById<TextView>(R.id.y1_24)
        val y1_36 = rootView.findViewById<TextView>(R.id.y1_36)
        val y1_40 = rootView.findViewById<TextView>(R.id.y1_40)

        y1cal.text = ratesArr["Y1"]?.get("CAL") ?: "-"
        y1work.text = ratesArr["Y1"]?.get("WRK") ?: "-"
        y1vac.text = ratesArr["Y1"]?.get("VAC") ?: "-"
        y1_24.text = ratesArr["Y1"]?.get("24") ?: "-"
        y1_36.text = ratesArr["Y1"]?.get("36") ?: "-"
        y1_40.text = ratesArr["Y1"]?.get("40") ?: "-"

        val q1cal = rootView.findViewById<TextView>(R.id.q1cal)
        val q1work = rootView.findViewById<TextView>(R.id.q1work)
        val q1vac = rootView.findViewById<TextView>(R.id.q1vac)
        val q1_24 = rootView.findViewById<TextView>(R.id.q1_24)
        val q1_36 = rootView.findViewById<TextView>(R.id.q1_36)
        val q1_40 = rootView.findViewById<TextView>(R.id.q1_40)

        q1cal.text = ratesArr["Q1"]?.get("CAL") ?: "-"
        q1work.text = ratesArr["Q1"]?.get("WRK") ?: "-"
        q1vac.text = ratesArr["Q1"]?.get("VAC") ?: "-"
        q1_24.text = ratesArr["Q1"]?.get("24") ?: "-"
        q1_36.text = ratesArr["Q1"]?.get("36") ?: "-"
        q1_40.text = ratesArr["Q1"]?.get("40") ?: "-"

        val q2cal = rootView.findViewById<TextView>(R.id.q2cal)
        val q2work = rootView.findViewById<TextView>(R.id.q2work)
        val q2vac = rootView.findViewById<TextView>(R.id.q2vac)
        val q2_24 = rootView.findViewById<TextView>(R.id.q2_24)
        val q2_36 = rootView.findViewById<TextView>(R.id.q2_36)
        val q2_40 = rootView.findViewById<TextView>(R.id.q2_40)

        q2cal.text = ratesArr["Q2"]?.get("CAL") ?: "-"
        q2work.text = ratesArr["Q2"]?.get("WRK") ?: "-"
        q2vac.text = ratesArr["Q2"]?.get("VAC") ?: "-"
        q2_24.text = ratesArr["Q2"]?.get("24") ?: "-"
        q2_36.text = ratesArr["Q2"]?.get("36") ?: "-"
        q2_40.text = ratesArr["Q2"]?.get("40") ?: "-"

        val h1cal = rootView.findViewById<TextView>(R.id.h1cal)
        val h1work = rootView.findViewById<TextView>(R.id.h1work)
        val h1vac = rootView.findViewById<TextView>(R.id.h1vac)
        val h1_24 = rootView.findViewById<TextView>(R.id.h1_24)
        val h1_36 = rootView.findViewById<TextView>(R.id.h1_36)
        val h1_40 = rootView.findViewById<TextView>(R.id.h1_40)

        h1cal.text = ratesArr["H1"]?.get("CAL") ?: "-"
        h1work.text = ratesArr["H1"]?.get("WRK") ?: "-"
        h1vac.text = ratesArr["H1"]?.get("VAC") ?: "-"
        h1_24.text = ratesArr["H1"]?.get("24") ?: "-"
        h1_36.text = ratesArr["H1"]?.get("36") ?: "-"
        h1_40.text = ratesArr["H1"]?.get("40") ?: "-"

        val q3cal = rootView.findViewById<TextView>(R.id.q3cal)
        val q3work = rootView.findViewById<TextView>(R.id.q3work)
        val q3vac = rootView.findViewById<TextView>(R.id.q3vac)
        val q3_24 = rootView.findViewById<TextView>(R.id.q3_24)
        val q3_36 = rootView.findViewById<TextView>(R.id.q3_36)
        val q3_40 = rootView.findViewById<TextView>(R.id.q3_40)

        q3cal.text = ratesArr["Q3"]?.get("CAL") ?: "-"
        q3work.text = ratesArr["Q3"]?.get("WRK") ?: "-"
        q3vac.text = ratesArr["Q3"]?.get("VAC") ?: "-"
        q3_24.text = ratesArr["Q3"]?.get("24") ?: "-"
        q3_36.text = ratesArr["Q3"]?.get("36") ?: "-"
        q3_40.text = ratesArr["Q3"]?.get("40") ?: "-"

        val q4cal = rootView.findViewById<TextView>(R.id.q4cal)
        val q4work = rootView.findViewById<TextView>(R.id.q4work)
        val q4vac = rootView.findViewById<TextView>(R.id.q4vac)
        val q4_24 = rootView.findViewById<TextView>(R.id.q4_24)
        val q4_36 = rootView.findViewById<TextView>(R.id.q4_36)
        val q4_40 = rootView.findViewById<TextView>(R.id.q4_40)

        q4cal.text = ratesArr["Q4"]?.get("CAL") ?: "-"
        q4work.text = ratesArr["Q4"]?.get("WRK") ?: "-"
        q4vac.text = ratesArr["Q4"]?.get("VAC") ?: "-"
        q4_24.text = ratesArr["Q4"]?.get("24") ?: "-"
        q4_36.text = ratesArr["Q4"]?.get("36") ?: "-"
        q4_40.text = ratesArr["Q4"]?.get("40") ?: "-"

        val h2cal = rootView.findViewById<TextView>(R.id.h2cal)
        val h2work = rootView.findViewById<TextView>(R.id.h2work)
        val h2vac = rootView.findViewById<TextView>(R.id.h2vac)
        val h2_24 = rootView.findViewById<TextView>(R.id.h2_24)
        val h2_36 = rootView.findViewById<TextView>(R.id.h2_36)
        val h2_40 = rootView.findViewById<TextView>(R.id.h2_40)

        h2cal.text = ratesArr["H2"]?.get("CAL") ?: "-"
        h2work.text = ratesArr["H2"]?.get("WRK") ?: "-"
        h2vac.text = ratesArr["H2"]?.get("VAC") ?: "-"
        h2_24.text = ratesArr["H2"]?.get("24") ?: "-"
        h2_36.text = ratesArr["H2"]?.get("36") ?: "-"
        h2_40.text = ratesArr["H2"]?.get("40") ?: "-"

        return rootView
    }

    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }


}// Required empty public constructor
