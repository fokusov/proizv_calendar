package com.fokusov.przvcal.Fragments

import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fokusov.przvcal.DB.EventModule

import com.fokusov.przvcal.Fragments.MyEventFragment.OnListFragmentInteractionListener
import com.fokusov.przvcal.Model.Event
import com.fokusov.przvcal.R
import java.text.SimpleDateFormat


class MyEventRecyclerViewAdapter(private val mListener: OnListFragmentInteractionListener?) :
        RecyclerView.Adapter<MyEventRecyclerViewAdapter.ViewHolder>() {

    private val EventList: MutableList<Event>
    private val dayFormat = SimpleDateFormat("dd.MM.yy")

    init {
        EventList = EventModule.allEvents()
        notifyDataSetChanged()
    }

    fun updateEvents(){
        EventList.clear()
        val evLst = EventModule.allEvents()
        evLst.forEach {
            EventList.add(it)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_myevent, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = EventList[position]
        holder.mIdView.text = dayFormat.format(EventList[position].date)
        holder.mContentView.text = EventList[position].desc

        if ((holder.mItem as Event).isEnabled) {
            holder.mIdView?.paintFlags = Paint.ANTI_ALIAS_FLAG
            holder.mContentView?.paintFlags = Paint.ANTI_ALIAS_FLAG
        } else {
            holder.mIdView?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            holder.mContentView?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        }
        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem!!)
        }
    }

    override fun getItemCount(): Int {
        return EventList.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView
        val mContentView: TextView
        var mItem: Event? = null

        init {
            mIdView = mView.findViewById(R.id.id) as TextView
            mContentView = mView.findViewById(R.id.content) as TextView
        }

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }

    }

}
