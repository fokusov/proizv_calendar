package com.fokusov.przvcal.Fragments


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.fokusov.przvcal.Controller.Cal

import com.fokusov.przvcal.R
import android.widget.AdapterView.OnItemSelectedListener
import com.fokusov.przvcal.Controller.CalendarActivity
import android.widget.Toast
import com.thebluealliance.spectrum.SpectrumDialog
import kotlinx.android.synthetic.main.fragment_settings.view.*


/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : Fragment() {

    private lateinit var myPref : SharedPreferences
    var check = 0

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        myPref = Cal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        val view = inflater!!.inflate(R.layout.fragment_settings, container, false)

        val cb_myev_is_vac = view.findViewById<CheckBox>(R.id.cb_myev_is_vac)
        val cb_show_today = view.findViewById<CheckBox>(R.id.cb_show_today)

        val vac_color4 = view.findViewById<ImageView>(R.id.pref_vac_color_4)
        val vac_color5 = view.findViewById<ImageView>(R.id.pref_vac_color_5)

        val app_coutry = view.findViewById<Spinner>(R.id.app_coutry)
        val app_theme = view.findViewById<Spinner>(R.id.app_theme)

        val pref_cb_myev_is_vac = myPref.getBoolean("cb_myev_is_vac", false)
        cb_myev_is_vac.isChecked = pref_cb_myev_is_vac

        val pref_cb_show_today = myPref.getBoolean("cb_show_today", false)
        cb_show_today.isChecked = pref_cb_show_today

        val pref_app_coutry = myPref.getInt("app_coutry", 0)
        app_coutry.setSelection(pref_app_coutry)

        val pref_app_theme = myPref.getInt("app_theme", 0)
        app_theme.setSelection(pref_app_theme)

        val pref_vac_color4 = myPref.getInt("day_color", Color.RED)
        vac_color4.setColorFilter(pref_vac_color4)

        val pref_vac_color5 = myPref.getInt("mydays_color", Color.BLUE)
        vac_color5.setColorFilter(pref_vac_color5)

        //listeners
        app_coutry.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                myPref.edit().putInt("app_coutry", position).apply()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        app_theme.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //val selectedItem = parent.getItemAtPosition(position).toString()
                //Toast.makeText(context, selectedItem, Toast.LENGTH_SHORT).show()
                myPref.edit().putInt("app_theme", position).apply()
                //save prefs and restart
                if (++check > 1) {
                    activity.finish()
                    val intent = Intent(context, CalendarActivity::class.java)
                    startActivity(intent)
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        vac_color4.setOnClickListener{
            SpectrumDialog.Builder(context)
                    .setTitle(R.string.select_color)
                    .setColors(R.array.demo_colors)
                    .setSelectedColor(pref_vac_color4)
                    .setDismissOnColorSelected(true)
                    .setOutlineWidth(2)
                    .setOnColorSelectedListener(SpectrumDialog.OnColorSelectedListener { positiveResult, color ->
                        if (positiveResult) {
//                            Toast.makeText(context, getString(R.string.color_set) + " #" + Integer.toHexString(color).toUpperCase(), Toast.LENGTH_SHORT).show()
                            vac_color4.setColorFilter(color)
                            myPref.edit().putInt("day_color", color).apply()
                        }
                    }).build().show(fragmentManager, "dialog_demo_1")
        }

        //my days
        vac_color5.setOnClickListener{
            SpectrumDialog.Builder(context)
                    .setTitle(R.string.select_color)
                    .setColors(R.array.demo_colors)
                    .setSelectedColor(pref_vac_color5)
                    .setDismissOnColorSelected(true)
                    .setOutlineWidth(2)
                    .setOnColorSelectedListener(SpectrumDialog.OnColorSelectedListener { positiveResult, color ->
                        if (positiveResult) {
//                            Toast.makeText(context, getString(R.string.color_set) + " #" + Integer.toHexString(color).toUpperCase(), Toast.LENGTH_SHORT).show()
                            vac_color5.setColorFilter(color)
                            myPref.edit().putInt("mydays_color", color).apply()
                        }
                    }).build().show(fragmentManager, "dialog_demo_1")
        }

        cb_show_today.setOnCheckedChangeListener { buttonView, isChecked ->
            myPref.edit().putBoolean("cb_show_today", isChecked).apply()
        }

        cb_myev_is_vac.setOnCheckedChangeListener { buttonView, isChecked ->
            myPref.edit().putBoolean("cb_myev_is_vac", isChecked).apply()
        }
        
        return view
    }

    private fun saveColor(color: Int){
        Toast.makeText(context, getString(R.string.color_set), Toast.LENGTH_SHORT).show()
        myPref.edit().putInt("vac_color", color).apply()
    }

}// Required empty public constructor
