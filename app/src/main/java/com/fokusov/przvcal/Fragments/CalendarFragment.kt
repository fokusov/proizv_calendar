package com.fokusov.przvcal.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fokusov.przvcal.Controller.Cal
import com.fokusov.przvcal.Controller.EventDecorator
import com.fokusov.przvcal.Controller.HighlightWeekendsDecorator
import com.fokusov.przvcal.DB.EventModule
import com.fokusov.przvcal.Model.Constants
import com.fokusov.przvcal.Model.Event
import com.fokusov.przvcal.Model.Event_Table.date

import com.fokusov.przvcal.R
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import org.json.JSONArray
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CalendarFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 */
class CalendarFragment : Fragment() {

    private var mListener: OnFragmentInteractionListener? = null
    val dayFormat = SimpleDateFormat("d MMMM yyyy, EEEE")
    val rangeDayFormat = SimpleDateFormat("d MMMM yyyy")
    val monthFormat = SimpleDateFormat("LLLL yyyy")
    private lateinit var myPref : SharedPreferences

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_calendar, container, false)

        myPref = Cal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        val prefAppCoutry = myPref.getInt("app_coutry", 0)

        val eventArrayFromJson = loadJSONFromAsset(prefAppCoutry)

        val events = ArrayList<CalendarDay>()
        val shortEvents = ArrayList<CalendarDay>()
        val myEvents = ArrayList<CalendarDay>()
        val allEvents = ArrayList<Event>()

        for (i in 0..(eventArrayFromJson.length() - 1)){
            val item = eventArrayFromJson.getJSONObject(i)
            val dateFormat = SimpleDateFormat("dd.MM.yyyy")
            val dateOfEvent = dateFormat.parse(item["date"].toString())
            var c = GregorianCalendar()
            c.time = dateOfEvent
            val dayType = item["type"] as Int

            val mEvent = Event(i, dateOfEvent,
                    item["type"] as Int, item["desc"] as String)
            if (dayType == 2) {
                shortEvents.add(CalendarDay(c))
            } else if (dayType == 9) {
                myEvents.add(CalendarDay(c))
            } else {
                events.add(CalendarDay(c))
            }

            allEvents.add(mEvent) //for range check
        }

        val myLocalEvents = EventModule.allActiveEvents()
        myLocalEvents.forEach {
            var c = GregorianCalendar()
            c.time = it.date
            myEvents.add(CalendarDay(c))
        }

        val calView = rootView.findViewById<MaterialCalendarView>(R.id.calendarView)
        val mInfo = rootView.findViewById<TextView>(R.id.monthInfo)

        calView.selectionMode = MaterialCalendarView.SELECTION_MODE_RANGE
        calView.topbarVisible = false

        val selColor = myPref.getInt("day_color", Color.RED)
        val myDaysColor = myPref.getInt("mydays_color", Color.BLUE)

        val app_theme = myPref.getInt("app_theme", 0)
        var strokeColor = selColor
        when(app_theme){
            0 -> strokeColor = Color.parseColor("#3d5392")
            1 -> strokeColor = Color.parseColor("#FF303030")
            2 -> strokeColor = Color.parseColor("#3d5392")
        }

        //calView.addDecorator(HighlightWeekendsDecorator(selColor)) //Color.parseColor("#$selColor")
        calView.addDecorator(EventDecorator(selColor, events, context, false, strokeColor))
        calView.addDecorator(EventDecorator(Color.GREEN, shortEvents, context, true, strokeColor))
        calView.addDecorator(EventDecorator(myDaysColor, myEvents, context, false, strokeColor))

        //highlight today
        var c = GregorianCalendar()

        val pref_cb_show_today = myPref.getBoolean("cb_show_today", false)
        if (pref_cb_show_today) {
            calView.setDateSelected(c.getTime(), true)
        }
        //
        calView.setOnDateChangedListener({ _, date, selected ->
            updateMonthInfo(date, rootView, 1, selected)
        })

        calView.setOnMonthChangedListener { widget, date ->
            updateMonthInfo(date, rootView, 2)
        }

        calView.setOnRangeSelectedListener { widget, dates ->
            val infoText = "${rangeDayFormat.format(dates[0].date)} - ${rangeDayFormat.format(dates[dates.size-1].date)}"
            mInfo.text = infoText
            updateRangeInfo(dates, rootView, allEvents)
        }

        updateMonthInfo(CalendarDay(c), rootView, 2)

        return rootView
    }

    private fun updateMonthInfo(calDate: CalendarDay, mView: View, type: Int, selected: Boolean = false) {
        val mInfo = mView.findViewById<TextView>(R.id.monthInfo)
        val cal = mView.findViewById<TextView>(R.id.cal)
        val work = mView.findViewById<TextView>(R.id.work)
        val vac = mView.findViewById<TextView>(R.id.vac)
        val m_24 = mView.findViewById<TextView>(R.id.m_24)
        val m_36 = mView.findViewById<TextView>(R.id.m_36)
        val m_40 = mView.findViewById<TextView>(R.id.m_40)

        if (type == 1) {
            if (selected){
                mInfo.text = dayFormat.format(calDate.date)
            } else {
                mInfo.text = monthFormat.format(calDate.date).toString().toUpperCase()
            }
        } else {
            mInfo.text = monthFormat.format(calDate.date).toString().toUpperCase()
        }

        if (calDate.year == Constants.APPYEAR) {
            cal.text = Constants.month_data_RU[calDate.month]?.get("total") ?: "-"
            work.text = Constants.month_data_RU[calDate.month]?.get("workdays") ?: "-"
            vac.text = Constants.month_data_RU[calDate.month]?.get("vacdays") ?: "-"
            m_24.text = Constants.month_data_RU[calDate.month]?.get("workhours24") ?: "-"
            m_36.text = Constants.month_data_RU[calDate.month]?.get("workhours36") ?: "-"
            m_40.text = Constants.month_data_RU[calDate.month]?.get("workhours40") ?: "-"
        } else {
            cal.text = "-"
            work.text = "-"
            vac.text = "-"
            m_24.text = "-"
            m_36.text = "-"
            m_40.text = "-"
        }
    }

    private fun updateRangeInfo(selectedDates: List<CalendarDay>, mView: View,
                                eventList: ArrayList<Event>) {
        val cal = mView.findViewById<TextView>(R.id.cal)
        val work = mView.findViewById<TextView>(R.id.work)
        val vac = mView.findViewById<TextView>(R.id.vac)
        val m_24 = mView.findViewById<TextView>(R.id.m_24)
        val m_36 = mView.findViewById<TextView>(R.id.m_36)
        val m_40 = mView.findViewById<TextView>(R.id.m_40)

        var mVac = 0 //vacation days count
        var shrt = 0 //short days count

        selectedDates.forEach {
            val calDay = it
            eventList.forEach {
                if (calDay.date == it.date){
                    when (it.type){
                        1 -> mVac++
                        2 -> shrt++
                        3 -> mVac++
                        //9 ->
                    }
                }
            }
        }

        val total = selectedDates.count()
        val mWrk = total - mVac     //work days count
        val fullDays = mWrk - shrt
        val m24 = (fullDays * 4.8 + shrt * 3.8)
        val m36 = (fullDays * 7.2 + shrt * 6.2)
        val m40 = (fullDays * 8.0 + shrt * 7.0)

        cal.text = total.toString()
        work.text = mWrk.toString()
        vac.text  = mVac.toString()
        m_24.text = String.format("%.1f", m24)
        m_36.text = String.format("%.1f", m36)
        m_40.text = String.format("%.1f", m40)

    }

    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    private fun loadJSONFromAsset(country: Int): JSONArray {
        var json: String?

        var fname = "cal_ru.json"
        when(country){
            0 -> fname = "cal_ru.json"
            1 -> fname = "cal_be.json"
            2 -> fname = "cal_ua.json"
        }

        try {
            json = context!!.assets.open(fname).bufferedReader().use {
                it.readText()
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
            json = "[]"
        }
        return JSONArray(json)

    }

}// Required empty public constructor
