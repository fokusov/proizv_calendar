package com.fokusov.przvcal.Fragments

import android.app.Dialog
import android.support.v4.app.DialogFragment
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import com.fokusov.przvcal.DB.EventModule
import com.fokusov.przvcal.Model.Event
import com.fokusov.przvcal.R
import java.util.*

/**
 * Created by Igor on 21.02.2018.
 */
class ModalFragment: DialogFragment() {

    var mEvId = -1
    lateinit var mEv: Event

    override fun onResume() {
        super.onResume()
        val modalTitle = dialog.findViewById<TextView>(R.id.modalTitle)
        val evEnabled = dialog.findViewById<CheckBox>(R.id.eventEnabled)

        if (mEvId > -1) {
            val calendar = Calendar.getInstance()
            val evName = dialog.findViewById<EditText>(R.id.eventDesc)
            val datePicker = dialog.findViewById<DatePicker>(R.id.eventDate)

            modalTitle.text = getString(R.string.event_edit)

            evEnabled.isChecked = mEv.isEnabled
            evName?.setText(mEv.desc, TextView.BufferType.EDITABLE)
            calendar.time = mEv.date
            (datePicker as DatePicker).init(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    DatePicker.OnDateChangedListener {
                        _, _, _, _ ->
                    })
        } else {
            modalTitle.text = getString(R.string.event_new)
            evEnabled.isChecked = true
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val rootView = inflater!!.inflate(R.layout.dialog_save, container, false)

        val datePicker = rootView.findViewById<DatePicker>(R.id.eventDate)
        val evName = rootView.findViewById<EditText>(R.id.eventDesc)
        val evEnabled = rootView.findViewById<CheckBox>(R.id.eventEnabled)
        val modalTitle = rootView.findViewById<TextView>(R.id.modalTitle)

        val arg = this.arguments
        if (arg != null){
            mEvId = arg["evId"] as Int
            mEv = EventModule.findEvent(mEvId) as Event
        }

        val calendar = Calendar.getInstance()

        (datePicker as DatePicker).init(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                DatePicker.OnDateChangedListener {
                    _, year, month, dayOfMonth ->
                        Log.d("DatePicker", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth)
                })

        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        builder.setView(R.layout.dialog_save)
               .setPositiveButton(getString(R.string.btn_save), DialogInterface.OnClickListener { dialog, which ->
                   saveData(mEvId)
               })
               .setNegativeButton(getString(R.string.btn_cancel), DialogInterface.OnClickListener { dialog, which ->  })
               .setNeutralButton(getString(R.string.btn_delete), DialogInterface.OnClickListener { dialog, which ->
                   deleteData(mEvId)
               })

        return builder.create()
    }

    private fun saveData(evId: Int){
        val evName = dialog.findViewById<EditText>(R.id.eventDesc)
        val datePicker = dialog.findViewById<DatePicker>(R.id.eventDate)
        val evEnabled = dialog.findViewById<CheckBox>(R.id.eventEnabled)

        if (mEvId == -1) {
            val mEvent = Event(0, Date(datePicker.year - 1900, datePicker.month, datePicker.dayOfMonth),
                    9, evName.text.toString(), evEnabled.isChecked)
            mEvent.save()
        } else {
            val mEvent = mEv
            mEvent.date = Date(datePicker.year - 1900, datePicker.month, datePicker.dayOfMonth)
            mEvent.desc = evName.text.toString()
            mEvent.isEnabled = evEnabled.isChecked
            mEvent.save()
        }

        if(targetFragment != null){
            targetFragment.onActivityResult(targetRequestCode, 7, activity.intent)
        }
        dismiss()
    }

    private fun deleteData(evId: Int){
        if (mEvId > -1) {
            val mEvent = mEv
            mEvent.delete()
            if(targetFragment != null){
                targetFragment.onActivityResult(targetRequestCode, 7, activity.intent)
            }
            dismiss()
        }
    }

}