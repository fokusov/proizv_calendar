package com.fokusov.przvcal.Model

/**
 * Created by Igor on 20.02.2018.
 */
object Constants {

    const val APPYEAR = 2018

    val rates_data_RU = mapOf<String, HashMap<String, String>>(
            "Y1" to hashMapOf<String, String>(
                    "CAL" to "365",
                    "WRK" to "247",
                    "VAC" to "118",
                    "24" to "1179,6",
                    "36" to "1772,4",
                    "40" to "1970"
            ),
            "Q1" to hashMapOf<String, String>(
                    "CAL" to "90",
                    "WRK" to "56",
                    "VAC" to "34",
                    "24" to "266,8",
                    "36" to "401,2",
                    "40" to "446"
            ),
            "Q2" to hashMapOf<String, String>(
                    "CAL" to "91",
                    "WRK" to "61",
                    "VAC" to "30",
                    "24" to "289,8",
                    "36" to "436,2",
                    "40" to "485"
            ),
            "Q3" to hashMapOf<String, String>(
                    "CAL" to "92",
                    "WRK" to "65",
                    "VAC" to "27",
                    "24" to "312",
                    "36" to "468",
                    "40" to "520"
            ),
            "Q4" to hashMapOf<String, String>(
                    "CAL" to "92",
                    "WRK" to "65",
                    "VAC" to "27",
                    "24" to "311",
                    "36" to "467",
                    "40" to "519"
            ),
            "H1" to hashMapOf<String, String>(
                    "CAL" to "181",
                    "WRK" to "117",
                    "VAC" to "64",
                    "24" to "556,6",
                    "36" to "837,4",
                    "40" to "931"
            ),
            "H2" to hashMapOf<String, String>(
                    "CAL" to "184",
                    "WRK" to "130",
                    "VAC" to "54",
                    "24" to "623",
                    "36" to "935",
                    "40" to "1039"
            )
    )

    val rates_data_UA = mapOf<String, HashMap<String, String>>(
            "Y1" to hashMapOf<String, String>(
                    "CAL" to "365",
                    "WRK" to "250",
                    "VAC" to "115",
                    "24" to "1196,4",
                    "36" to "1794,6",
                    "40" to "1994"
            ),
            "Q1" to hashMapOf<String, String>(
                    "CAL" to "90",
                    "WRK" to "62",
                    "VAC" to "28",
                    "24" to "297",
                    "36" to "445,5",
                    "40" to "495"
            ),
            "Q2" to hashMapOf<String, String>(
                    "CAL" to "91",
                    "WRK" to "59",
                    "VAC" to "32",
                    "24" to "281,4",
                    "36" to "422,1",
                    "40" to "469"
            ),
            "Q3" to hashMapOf<String, String>(
                    "CAL" to "92",
                    "WRK" to "64",
                    "VAC" to "28",
                    "24" to "306,6",
                    "36" to "459,9",
                    "40" to "511"
            ),
            "Q4" to hashMapOf<String, String>(
                    "CAL" to "92",
                    "WRK" to "65",
                    "VAC" to "27",
                    "24" to "311,4",
                    "36" to "469,1",
                    "40" to "519"
            ),
            "H1" to hashMapOf<String, String>(
                    "CAL" to "181",
                    "WRK" to "121",
                    "VAC" to "60",
                    "24" to "578,4",
                    "36" to "867,6",
                    "40" to "964"
            ),
            "H2" to hashMapOf<String, String>(
                    "CAL" to "184",
                    "WRK" to "129",
                    "VAC" to "55",
                    "24" to "618",
                    "36" to "927",
                    "40" to "1030"
            )
    )

    val rates_data_BE = mapOf<String, HashMap<String, String>>(
            "Y1" to hashMapOf<String, String>(
                    "CAL" to "365",
                    "WRK" to "253",
                    "VAC" to "112",
                    "24" to "-",
                    "36" to "-",
                    "40" to "2016"
            ),
            "Q1" to hashMapOf<String, String>(
                    "CAL" to "90",
                    "WRK" to "63",
                    "VAC" to "27",
                    "24" to "-",
                    "36" to "-",
                    "40" to "503"
            ),
            "Q2" to hashMapOf<String, String>(
                    "CAL" to "91",
                    "WRK" to "62",
                    "VAC" to "29",
                    "24" to "-",
                    "36" to "-",
                    "40" to "493"
            ),
            "Q3" to hashMapOf<String, String>(
                    "CAL" to "92",
                    "WRK" to "64",
                    "VAC" to "28",
                    "24" to "-",
                    "36" to "-",
                    "40" to "511"
            ),
            "Q4" to hashMapOf<String, String>(
                    "CAL" to "92",
                    "WRK" to "64",
                    "VAC" to "28",
                    "24" to "-",
                    "36" to "-",
                    "40" to "509"
            ),
            "H1" to hashMapOf<String, String>(
                    "CAL" to "181",
                    "WRK" to "125",
                    "VAC" to "56",
                    "24" to "-",
                    "36" to "-",
                    "40" to "996"
            ),
            "H2" to hashMapOf<String, String>(
                    "CAL" to "184",
                    "WRK" to "128",
                    "VAC" to "56",
                    "24" to "-",
                    "36" to "-",
                    "40" to "1020"
            )
    )
    
    /// months
    val month_data_RU = mapOf<Int, HashMap<String, String>>(
        0 to hashMapOf<String, String>(
            "total" to "31",
            "workdays" to "17",
            "vacdays" to "14",
            "workhours24" to "81,6",
            "workhours36" to "122,4",
            "workhours40" to "136"
        ),
        1 to hashMapOf<String, String>(
            "total" to "28",
            "workdays" to "19",
            "vacdays" to "9",
            "workhours24" to "90,2",
            "workhours36" to "135,8",
            "workhours40" to "151"
        ),
        2 to hashMapOf<String, String>(
                "total" to "31",
                "workdays" to "20",
                "vacdays" to "11",
                "workhours24" to "95",
                "workhours36" to "143",
                "workhours40" to "159"
        ),
        3 to hashMapOf<String, String>(
                "total" to "30",
                "workdays" to "21",
                "vacdays" to "9",
                "workhours24" to "99,8",
                "workhours36" to "150,2",
                "workhours40" to "167"
        ),
        4 to hashMapOf<String, String>(
                "total" to "31",
                "workdays" to "20",
                "vacdays" to "11",
                "workhours24" to "95",
                "workhours36" to "143",
                "workhours40" to "159"
        ),
        5 to hashMapOf<String, String>(
                "total" to "30",
                "workdays" to "20",
                "vacdays" to "10",
                "workhours24" to "95",
                "workhours36" to "143",
                "workhours40" to "159"
        ),
        6 to hashMapOf<String, String>(
                "total" to "31",
                "workdays" to "22",
                "vacdays" to "9",
                "workhours24" to "105,6",
                "workhours36" to "158,4",
                "workhours40" to "176"
        ),
        7 to hashMapOf<String, String>(
                "total" to "31",
                "workdays" to "23",
                "vacdays" to "8",
                "workhours24" to "110,4",
                "workhours36" to "165,6",
                "workhours40" to "184"
        ),
        8 to hashMapOf<String, String>(
                "total" to "30",
                "workdays" to "20",
                "vacdays" to "10",
                "workhours24" to "96",
                "workhours36" to "144",
                "workhours40" to "160"
        ),
        9 to hashMapOf<String, String>(
                "total" to "31",
                "workdays" to "23",
                "vacdays" to "8",
                "workhours24" to "110,4",
                "workhours36" to "165,6",
                "workhours40" to "184"
        ),
        10 to hashMapOf<String, String>(
                "total" to "30",
                "workdays" to "21",
                "vacdays" to "9",
                "workhours24" to "100,8",
                "workhours36" to "151,2",
                "workhours40" to "168"
        ),
        11 to hashMapOf<String, String>(
                "total" to "31",
                "workdays" to "21",
                "vacdays" to "10",
                "workhours24" to "99,8",
                "workhours36" to "150,2",
                "workhours40" to "167"
        )
    )

    val month_data_UA = mapOf<Int, HashMap<String, String>>(
            0 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "21",
                    "vacdays" to "10",
                    "workhours24" to "100,8",
                    "workhours36" to "151,2",
                    "workhours40" to "168"
            ),
            1 to hashMapOf<String, String>(
                    "total" to "28",
                    "workdays" to "20",
                    "vacdays" to "8",
                    "workhours24" to "96",
                    "workhours36" to "144",
                    "workhours40" to "160"
            ),
            2 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "21",
                    "vacdays" to "10",
                    "workhours24" to "100,2",
                    "workhours36" to "150,3",
                    "workhours40" to "167"
            ),
            3 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "20",
                    "vacdays" to "10",
                    "workhours24" to "95,4",
                    "workhours36" to "143,1",
                    "workhours40" to "159"
            ),
            4 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "19",
                    "vacdays" to "12",
                    "workhours24" to "90,6",
                    "workhours36" to "135,9",
                    "workhours40" to "151"
            ),
            5 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "20",
                    "vacdays" to "10",
                    "workhours24" to "95,4",
                    "workhours36" to "143,1",
                    "workhours40" to "159"
            ),
            6 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "22",
                    "vacdays" to "9",
                    "workhours24" to "105,6",
                    "workhours36" to "158,4",
                    "workhours40" to "176"
            ),
            7 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "22",
                    "vacdays" to "9",
                    "workhours24" to "105",
                    "workhours36" to "157,5",
                    "workhours40" to "175"
            ),
            8 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "20",
                    "vacdays" to "10",
                    "workhours24" to "96",
                    "workhours36" to "144",
                    "workhours40" to "160"
            ),
            9 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "22",
                    "vacdays" to "9",
                    "workhours24" to "105,6",
                    "workhours36" to "158,4",
                    "workhours40" to "176"
            ),
            10 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "22",
                    "vacdays" to "8",
                    "workhours24" to "105,6",
                    "workhours36" to "158,4",
                    "workhours40" to "176"
            ),
            11 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "21",
                    "vacdays" to "10",
                    "workhours24" to "100,2",
                    "workhours36" to "150,3",
                    "workhours40" to "167"
            )
    )

    val month_data_BE = mapOf<Int, HashMap<String, String>>(
            0 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "22",
                    "vacdays" to "9",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "176"
            ),
            1 to hashMapOf<String, String>(
                    "total" to "28",
                    "workdays" to "20",
                    "vacdays" to "8",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "160"
            ),
            2 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "21",
                    "vacdays" to "10",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "167"
            ),
            3 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "20",
                    "vacdays" to "10",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "158"
            ),
            4 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "21",
                    "vacdays" to "10",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "167"
            ),
            5 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "21",
                    "vacdays" to "9",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "168"
            ),
            6 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "21",
                    "vacdays" to "10",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "167"
            ),
            7 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "23",
                    "vacdays" to "8",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "184"
            ),
            8 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "20",
                    "vacdays" to "10",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "160"
            ),
            9 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "23",
                    "vacdays" to "8",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "184"
            ),
            10 to hashMapOf<String, String>(
                    "total" to "30",
                    "workdays" to "21",
                    "vacdays" to "9",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "167"
            ),
            11 to hashMapOf<String, String>(
                    "total" to "31",
                    "workdays" to "20",
                    "vacdays" to "11",
                    "workhours24" to "-",
                    "workhours36" to "-",
                    "workhours40" to "158"
            )
    )
}