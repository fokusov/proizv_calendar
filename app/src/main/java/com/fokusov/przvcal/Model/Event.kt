package com.fokusov.przvcal.Model

import com.fokusov.przvcal.DB.CalDataBase
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Igor on 15.02.2018.
 */
@Table(name = "Events", database = CalDataBase::class, allFields = true)
class Event (@PrimaryKey (autoincrement = true) var id : Int = 0,
             var date: Date = Date(0, 0, 0),
             var type: Int = 0,
             var desc: String = "",
             var isEnabled: Boolean = true) : BaseModel() {



    override fun toString(): String {
        val eventDescription = "${day()} $desc"
        return eventDescription
    }

    fun day() : String {
        val format = SimpleDateFormat("dd.MM.yy")
        return format.format(date)
    }

}