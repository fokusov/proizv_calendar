package com.fokusov.przvcal.DB

import com.raizlabs.android.dbflow.annotation.Database

/**
 * Created by Igor on 23.02.2018.
 */
@Database(version = CalDataBase.VERSION, name = CalDataBase.NAME)
object CalDataBase {
    const val VERSION = 1
    const val NAME = "cal"
}