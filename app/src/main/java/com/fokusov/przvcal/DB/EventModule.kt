package com.fokusov.przvcal.DB

import com.fokusov.przvcal.Model.Event
import com.fokusov.przvcal.Model.Event_Table
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.sql.language.Select
import java.util.*

/**
 * Created by Igor on 23.02.2018.
 */
object EventModule {

    fun allActiveEvents(): MutableList<Event>{
        return Select().from(Event::class.java).where(Event_Table.isEnabled.eq(true)).list
    }

    fun allActiveEventsByType(type: Int): MutableList<Event>{
        return Select()
                .from(Event::class.java)
                .where(Event_Table.isEnabled.eq(true))
                .and(Event_Table.type.eq(type)).list
    }

    fun allEvents(): MutableList<Event>{
        return Select().from(Event::class.java).list
    }

    fun findEvent(id: Int): Event? {
        return Select().from(Event::class.java).where(Event_Table.id.eq(id)).querySingle()
    }

    fun listFilteredEvents(year: Int, month: Int): MutableList<Event>{
        val monthStart = getDate(month, year, true)
        val monthEnd = getDate(month, year, false)

        return Select().from(Event::class.java)
                .where(Event_Table.date.between(monthStart).and(monthEnd))
                .queryList()
    }

    private fun getDate(month: Int, year: Int, start: Boolean): Date {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, 1)
        if (start) {
            calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE))
        } else {
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
        }
        return calendar.time
    }

}