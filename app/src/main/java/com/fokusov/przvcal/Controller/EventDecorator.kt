package com.fokusov.przvcal.Controller

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.shapes.OvalShape
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import com.fokusov.przvcal.R
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import com.prolificinteractive.materialcalendarview.spans.DotSpan
import java.util.ArrayList


/**
 * Created by Igor on 15.02.2018.
 */
class EventDecorator(val color: Int, val dates: ArrayList<CalendarDay>, val ctx: Context, val isShort: Boolean = false, val strokeColor: Int) : DayViewDecorator {

    var shape: GradientDrawable

    init{
        shape = GradientDrawable()
        //shape.cornerRadius = 5f
        shape.shape = GradientDrawable.OVAL
        shape.setColor(color)
//        shape.setSize(1,1)
//        shape.setSize(pxfromDp(10.0F),pxfromDp(10.0F))
//        shape.setSize(5,55)
        shape.setStroke(5, strokeColor)
    }

//    fun pxfromDp(dp: Float): Int {
//        return (dp * ctx.resources.displayMetrics.density).toInt()
//    }

    override fun shouldDecorate(day: CalendarDay?): Boolean {
        return dates.contains(day)
    }

    override fun decorate(view: DayViewFacade?) {
        //            view?.addSpan(ForegroundColorSpan(Color.WHITE))
        //            view?.addSpan(StyleSpan(Typeface.BOLD))
        //            view?.addSpan(RelativeSizeSpan(1.5f))
        if (isShort) view?.addSpan(DotSpan(5F, color)) else view?.setBackgroundDrawable(shape)
        //
//        var drawable = ctx.resources.getDrawable(R.drawable.my_date_selector)
//        val drawable = ColorDrawable(Color.parseColor("#1e88e5"))
//        when (color) {
//            0 -> drawable = ctx.resources.getDrawable(R.drawable.my_date_selector)
//            1 -> drawable = ctx.resources.getDrawable(R.drawable.my_date_selector_red)
//            2 -> drawable = ctx.resources.getDrawable(R.drawable.my_date_selector_green)
//            3 -> drawable = ctx.resources.getDrawable(R.drawable.my_date_selector_accent)
//        }

    }

}