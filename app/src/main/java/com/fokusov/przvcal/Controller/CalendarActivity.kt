package com.fokusov.przvcal.Controller

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceActivity
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.fokusov.przvcal.Fragments.*
import com.fokusov.przvcal.Model.Event
import com.fokusov.przvcal.R
import kotlinx.android.synthetic.main.activity_calendar.*

class CalendarActivity : AppCompatActivity(), CalendarFragment.OnFragmentInteractionListener,
            RatesFragment.OnFragmentInteractionListener, MyEventFragment.OnListFragmentInteractionListener {

    private lateinit var myPref : SharedPreferences

    override fun onListFragmentInteraction(item: Event) {
        val modalFragment = ModalFragment()
        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.findFragmentByTag("myEventFragment")
        modalFragment.setTargetFragment(ft, 1)

        val tempBundle = Bundle()
        tempBundle.putInt("evId", item.id)
        modalFragment.arguments = tempBundle

        modalFragment.show(fragmentManager, "modal")
    }

    override fun onFragmentInteraction(uri: Uri) {
        //
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        when (item.itemId) {
            R.id.navigation_home -> {
                val calendarFragment = CalendarFragment()
                fragmentTransaction.replace(R.id.frame_layout_fragment_container,
                        calendarFragment, "calendarFragment").commit()
                supportFragmentManager.executePendingTransactions()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_rates -> {
                val ratesFragment = RatesFragment()
                fragmentTransaction.replace(R.id.frame_layout_fragment_container,
                        ratesFragment, "ratesFragment").commit()
                supportFragmentManager.executePendingTransactions()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_myevents -> {
                val myEventFragment = MyEventFragment()
                fragmentTransaction.replace(R.id.frame_layout_fragment_container,
                        myEventFragment, "myEventFragment").commit()
                supportFragmentManager.executePendingTransactions()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                val mySettingsFragment = SettingsFragment()
                fragmentTransaction.replace(R.id.frame_layout_fragment_container,
                        mySettingsFragment, "mySettingsFragment").commit()
                supportFragmentManager.executePendingTransactions()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }



    override fun onCreate(savedInstanceState: Bundle?) {

        myPref = Cal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        val app_theme = myPref.getInt("app_theme", 0)

        when(app_theme){
            0 -> setTheme(R.style.MyLight)
            1 -> setTheme(R.style.MyDark)
            2 -> setTheme(R.style.MyWhite)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val calendarFragment = CalendarFragment()
        fragmentTransaction.replace(R.id.frame_layout_fragment_container,
                calendarFragment).commit()
    }
}
