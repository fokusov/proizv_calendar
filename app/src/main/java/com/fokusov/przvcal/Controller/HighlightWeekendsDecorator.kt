package com.fokusov.przvcal.Controller

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import java.util.*
import android.graphics.drawable.GradientDrawable



/**
 * Created by Igor on 09.03.2018.
 */
class HighlightWeekendsDecorator(val color: Int): DayViewDecorator {


    private var calendar = Calendar.getInstance()
    var shape: GradientDrawable

    init{
        shape = GradientDrawable()
        shape.cornerRadius = 25f
        shape.setColor(color)
    }

    override fun shouldDecorate(day: CalendarDay?): Boolean {
        day?.copyTo(calendar)
        var weekDay = calendar.get(Calendar.DAY_OF_WEEK)
        return weekDay == Calendar.SATURDAY || weekDay == Calendar.SUNDAY
    }

    override fun decorate(view: DayViewFacade?) {
        view?.setBackgroundDrawable(shape)
    }
}