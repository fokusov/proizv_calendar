package com.fokusov.przvcal.Controller

import android.app.Application
import android.content.Context
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager

/**
 * Created by Igor on 22.02.2018.
 */
class Cal : Application() {
    companion object {
        var ctx: Context? = null
    }
    override fun onCreate() {
        super.onCreate()
        FlowManager.init(FlowConfig.Builder(this).openDatabasesOnInit(true).build())
        ctx = applicationContext
    }

    override fun onTerminate() {
        super.onTerminate()
        FlowManager.destroy()
    }

}